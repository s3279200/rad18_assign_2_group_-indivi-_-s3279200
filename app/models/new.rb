class New < ApplicationRecord
  has_many :comments
  validates :source, allow_blank: true, format: { with: /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)/, message: 'is invalid url' }
  validates :title, length: { in: 10...200 }
  belongs_to :user
end
