class User < ApplicationRecord
  has_many :new
  has_many :comments

  validates :password, format: { with: /\A[-a-zA-Z\d_]+\z/, message: 'only contain letters, digits, dashes and underscores' }, length: { minimum: 10 }
      validates :username, length: { in: 2...15 }, uniqueness: { message: ' was used by anthoer' }
  has_secure_password
end
