class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :signed_user
  before_action :authencation

  def authencation
    if signed_user.blank?
      redirect_to login_index_path, notice: "Login first"
    end
  end

  def login_user(user)
    session[:user_id] = user.id
  end

  def logout_user
    session[:user_id] = nil
  end

  def signed_user
    @signed_user ||= User.find_by_id session[:user_id]
  end
end
