class NewcommentsController < ApplicationController
  def index
    @comments = Comment.all.limit(3).order(created_at: :desc).includes(:user, :new)
  end
end
