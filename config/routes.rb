Rails.application.routes.draw do
  root :to => 'news#index'
  resources :comments
  resources :newcomments
  resources :users do
    collection do
      get :logout
      get :about
    end
  end
  resources :news
  resources :submit
  resources :login
  namespace :v0 do
    resources :comment
    resources :new
  end
end
